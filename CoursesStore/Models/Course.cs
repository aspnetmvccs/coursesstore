﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CoursesStore.Models
{
    public class Course
    {
        public int CourseId { get; set; }
        public int CategoryId { get; set;  }
        [Required(ErrorMessage = "Add course title")]
        [StringLength(100)]
        public string CourseTitle { get; set; }
        [Required(ErrorMessage = "Add course author")]
        [StringLength(100)]
        public string CourseAuthor { get; set; }
        public string CourseDescrpition { get; set; }
        public string CourseOpinion { get; set; }
        [StringLength(100)]
        public string ImageFileName { get; set; }
        public DateTime DateAdded { get; set; }
        public decimal CoursePrice { get; set; }
        public short DiscountPercentage { get; set; }
        public bool Bestseller { get; set; }
        public bool Hidden { get; set; }
        public string ShortDescription { get; set; }
        public string Test { get; set; }
        public bool Recommended { get; set; }

        public virtual Category Category { get; set; }
    }
}