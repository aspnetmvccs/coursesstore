﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CoursesStore.Models
{
    public class Category
    {
        public int CategoryId { get; set;  }
        [Required(ErrorMessage = "Add category Name")]
        [StringLength(100)]
        public string CategoryName { get; set; }
        [Required(ErrorMessage = "Add category Description")]
        public string CategoryDescription { get; set; }
        public string IconFileName { get; set; }

        // umozliwia przechowywanie wielu kursow
        public virtual ICollection<Course> Courses { get; set; }

    }
}