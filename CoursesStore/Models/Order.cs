﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CoursesStore.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        [Required(ErrorMessage = "Add order Name")]
        [StringLength(100)]
        public string Imie { get; set; }
        [Required(ErrorMessage = "Add order Lastname")]
        [StringLength(100)]
        public string Nazwisko { get; set; }
        [Required(ErrorMessage = "Add order Street")]
        [StringLength(100)]
        public string Street { get; set; }
        [Required(ErrorMessage = "Add order City")]
        [StringLength(100)]
        public string City { get; set; }
        [Required(ErrorMessage = "Add order Zip code")]
        [StringLength(100)]
        public string PostalCode { get; set; }
        [RegularExpression(@"(\+\d{2})*[\d\s-]+", ErrorMessage = "bad Phonenumber")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Add order email")]
        [EmailAddress(ErrorMessage = "wrong email address")]
        public string Email { get; set; }
        public string Comment { get; set; }
        public DateTime DateOrder { get; set; }
        public decimal OrderValue { get; set; }

        public OrderStatus Status { get; set; }

        public List<OrderItem> OrderItems { get; set; }

        public enum OrderStatus
        {
            New,
            Done
        }
    }
}