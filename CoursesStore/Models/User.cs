﻿using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace CoursesStore.Models
{
    public  class User : IUser<int>
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
