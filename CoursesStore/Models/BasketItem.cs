﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoursesStore.Models
{
    public class BasketItem
    {
        public Course Course { get; set; }
        public int Count { get; set; }
        public decimal Value { get; set; }
       

        public decimal TotalPrice { get { return Count * Value; }  }
    }
}