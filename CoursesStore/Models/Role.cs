﻿using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace CoursesStore.Models
{
    public  class Role : IRole<int>
    {
        public Role()
        {
            this.Users = new HashSet<User>();
        }
            
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
