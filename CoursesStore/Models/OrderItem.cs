﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoursesStore.Models
{
    public class OrderItem
    {
        public int OrderItemId { get; set; } //primary key
        public int OrderId { get; set; } //foregin key
        public int CourseId { get; set; } //foregin key
        public int Count { get; set; }
        public decimal OrderPrice { get; set; } 

        public virtual Course course { get; set; }
        public virtual Order order { get; set; }
    }
}