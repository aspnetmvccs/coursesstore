﻿
        $(function (){
            $(".add-to-cart").click(function () {
                var addRecord = $(this).attr("data-id");
                if (addRecord != '') {
                    $.post("/Courses/AddToBasket", { "courseId": addRecord },
                        function (response) {
                            if (response.CountBasketItem > 0) {
                                $('#BucketPrice').text(parseFloat(response.SumBasketPrice).toFixed(2));
                                $('#BucketCount').text(response.CountBasketItem);

                                var button = $('.add-to-cart[data-id=' + response.AddItemId + ']');
                                $(button).hide();
                            }
                        });
                  //  return false;
                }
            });
        });
  