﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CoursesStore.Startup))]
namespace CoursesStore
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}