﻿using System;
using CoursesStore.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoursesStore.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Course> News { get; set; }
        public IEnumerable<Course> Bestsellers { get; set; }
    }
}