﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoursesStore.ViewModels
{
    public class RemoveViewModel
    {
        public int RemoveItem { get; set; }
        public int CountBasketItem { get; set; }
        public decimal SumBasketPrice { get; set; }
        public int IdRemovedItem { get; set; }
    }
}