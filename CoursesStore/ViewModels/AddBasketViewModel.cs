﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoursesStore.ViewModels
{
    public class AddBasketViewModel
    {
        public int AddItemId { get; set; }
        public int CountBasketItem { get; set; }
        public decimal SumBasketPrice { get; set; }
       
    }
}