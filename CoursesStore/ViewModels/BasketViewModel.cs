﻿using CoursesStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoursesStore.ViewModels
{
    public class BasketViewModel
    {
        public List<BasketItem> BasketItem { get; set; }
        public decimal TotalPrice { get; set; }
    }
}