﻿using CoursesStore.DAL;
using CoursesStore.Infrastructure;
using CoursesStore.Models;
using CoursesStore.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoursesStore.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private CoursesContext db = new CoursesContext();

        public ActionResult Index()
        {
            ICache cache = new DefaultCache();
            List<Category> category;

            if (cache.IsSet(Consts.CategoryCache))
            {
                category = cache.Get(Consts.CategoryCache) as List<Category>;
            }
            else
            {
                category = db.Categories.ToList();
                cache.Set(Consts.CategoryCache, category, 40);
            }

            //var category = db.Categories.ToList();
            var news = db.Courses.Where(a => !a.Hidden).OrderByDescending(a => a.DateAdded).Take(4).ToList();
            var bestsellers = db.Courses.Where(b => !b.Hidden && b.Bestseller).OrderBy(b => Guid.NewGuid()).Take(4).ToList();

            var vm = new HomeViewModel()
            {
                Categories = category,
                News = news,
                Bestsellers = bestsellers
            };

            return View(vm);
        }


        public ActionResult StaticSites(string name)
        {
            return View(name);
        }

        public ActionResult Lista()
        {
            var category = db.Categories.ToList();

            var vm = new HomeViewModel()
            {
                Categories = category
            };

            return View(vm);
        }
    }
}