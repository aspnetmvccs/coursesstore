﻿using CoursesStore.Infrastructure;
using CoursesStore.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoursesStore.Controllers
{
    public class BasketController : Controller
    {

        private BasketManager BM;

        public BasketController()
        {
            BM = new BasketManager(new SessionManager(), new DAL.CoursesContext());
        }

        public object BasektViewMode { get; private set; }

        // GET: Basket
        public ActionResult Index()
        {
            var basketItems = BM.GetBasket();

            BasketViewModel basketVM = new BasketViewModel()
            {
                BasketItem = basketItems,
                TotalPrice = BM.SumBasketPrice
            };

            return View(basketVM);
        }



        public ActionResult RemoveRecrod(int courseId)
        {
            var removeItemBasket = BM.RemoveItem(courseId);

            return UpdateBasketUI();
        }

        public ActionResult UpdateRecord(int courseId, int quantity)
        {
            BM.UpdateQuantity(courseId, quantity);

            return UpdateBasketUI();
        }

        private ActionResult UpdateBasketUI()
        {
            var countBasketItemBasket = BM.CountBasketItem();
            var sumBasketPriceBasket = BM.SumBasketPrice;
            var sumBasketPriceAfterDiscountBasket = BM.SumBasketPriceAfterDiscount;

            var basketItems = BM.GetBasket();
            string viewContent = ConvertViewToString("_BasketItems", basketItems);

            return Json(new { itemsHTML = viewContent, CountBasketItem = countBasketItemBasket, NetBasketPrice = sumBasketPriceAfterDiscountBasket, SumBasketPrice = sumBasketPriceBasket });

        }

        private string ConvertViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter writer = new StringWriter())
            {
                ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }

        public decimal GetPriceBasket()
        {
            decimal Total = BM.SumBasketPrice;
            return Total;
        }

        public decimal GetNetPriceBasket()
        {
            decimal Total = BM.SumBasketPriceAfterDiscount;
            return Total;
        }

        public int CountBasket()
        {
            int Counter = BM.CountBasketItem();
            return Counter;
        }

    }
}