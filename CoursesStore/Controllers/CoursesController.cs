﻿using CoursesStore.DAL;
using CoursesStore.Infrastructure;
using CoursesStore.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoursesStore.Controllers
{
    public class CoursesController : Controller
    {
        private CoursesContext db = new CoursesContext();
        // GET: Courses
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult Lists(string categoryName)
        //{
        //    List<Models.Course> courses;
        //    if (!string.IsNullOrEmpty(categoryName))
        //    {
        //        var category = db.Categories.Include("Courses").Where(k => k.CategoryName.ToUpper() == categoryName.ToUpper()).SingleOrDefault();
        //        courses = category == null ? new List<Models.Course>() : category.Courses.ToList();
        //    }
        //    else
        //        courses = db.Courses.ToList();

        //    return View(courses);
        //}

        public ActionResult Lists(string categoryName, string searchQ, int filterId = 0)
        {
            List<Models.Course> courses = new List<Models.Course>();

            if (!string.IsNullOrEmpty(categoryName) && filterId > 0)
            {
                categoryName = string.IsNullOrEmpty(categoryName) ? Session["CategoryName"].ToString() : categoryName;
                var category = db.Categories.Include("Courses").Where(k => k.CategoryName.ToUpper() == categoryName.ToUpper()).SingleOrDefault();

                if (category != null)
                {
                    switch (filterId)
                    {
                        case 1:
                            courses = category.Courses.Where(c => c.CoursePrice > 20).ToList();
                            break;
                        case 2:
                            courses = category.Courses.Where(c => c.Bestseller = true).ToList();
                            break;
                          
                        default:
                            courses = category.Courses.ToList();
                            break;
                    }

                }
                else if (filterId > 0)
                {
                    Session["CategoryName"] = string.Empty;
                    switch (filterId)
                    {
                        case 1:
                            courses = category.Courses.Where(c => c.CoursePrice > 20).ToList();
                            break;
                        case 2:
                            courses = category.Courses.Where(c => c.Bestseller = true).ToList();
                            break;
                        default:
                            courses = category.Courses.ToList();
                            break;
                    }

                }
                else if (!string.IsNullOrEmpty(categoryName))
                {
                    Session["CategoryName"] = categoryName;
                    var category = db.Categories.Include("Courses").Where(k => k.CategoryName.ToUpper() == categoryName.ToUpper()).SingleOrDefault();
                    if (category != null)
                        courses = category.Courses.ToList();
                }
                else if (!string.IsNullOrEmpty(searchQ))
                {
                    Session["CategoryName"] = categoryName;
                    courses = db.Courses.Where(c => c.CourseTitle.Contains(searchQ)).ToList();
                }
                else
                    courses = db.Courses.ToList();

                return View(courses);
            }



            public ActionResult Details(int? coursesId)
            {
                if (coursesId == null)
                    return RedirectToAction("Index", "Home");

                var course = db.Courses.Find(coursesId);
                if (course == null)
                    return RedirectToAction("Index", "Home");

                return View(course);
            }

            public ActionResult AddToBasket(int courseId)
            {
                BasketManager BM = new BasketManager(new SessionManager(), new DAL.CoursesContext());

                BM.AddBasket(courseId);

                var countBasketItemBasket = BM.CountBasketItem();
                var sumBasketPriceBasket = BM.SumBasketPrice;

                var addVM = new AddBasketViewModel()
                {
                    CountBasketItem = countBasketItemBasket,
                    SumBasketPrice = sumBasketPriceBasket,
                    AddItemId = courseId
                };

                return Json(addVM);

                // return RedirectToAction("Index");
            }

        [ChildActionOnly]
        [OutputCache(Duration = 60000)]
        public ActionResult CategoriesMenu()
        {
            var categories = db.Categories.ToList();
            return PartialView("_CategoriesMenu", categories);
        }

        [ChildActionOnly]
        public ActionResult Recommended()
        {
            var course = db.Courses.Where(r => r.Recommended == true).OrderBy(r => Guid.NewGuid()).Take(2).ToList();
            return PartialView("_Recommended", course);
        }

        public ActionResult CoursesHints(string term)
        {
            var courses = db.Courses.Where(a => !a.Hidden && a.CourseTitle.ToLower().Contains(term.ToLower())).Take(4).Select(a => new { label = a.CourseTitle, courseId = a.CourseId });
            return Json(courses, JsonRequestBehavior.AllowGet);
        }
    }
}