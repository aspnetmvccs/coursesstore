﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CoursesStore
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "CoursesLists",
                url: "Category/{categoryName}",
                defaults: new { controller = "Courses", action = "Lists" }
                );

            routes.MapRoute(
                 name: "CoursesDetails",
                 url: "course-{coursesId}.html",
                 defaults: new { controller = "Courses", action = "Details" });

            routes.MapRoute(
                name: "StaticSites",
                url: "site/{name}.html",
                defaults: new { controller = "Home", action = "StaticSites" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
