﻿using CoursesStore.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace CoursesStore.DAL
{
    public class CoursesContext : DbContext
    {
        // konstruktor
        public CoursesContext(): base("CoursesContext")
        {

        }
        static CoursesContext()
        {
            Database.SetInitializer<CoursesContext>(new CoursesInitializer());
        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        public  DbSet<Role> Roles { get; set; }
        public  DbSet<User> Users { get; set; }

        // metoda usuwajaca końcówki w nazwach tabel bd
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}