﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using CoursesStore.Models;
using System.Linq;
using System.Web;
using CoursesStore.Migrations;
using System.Data.Entity.Migrations;

namespace CoursesStore.DAL
{
    public class CoursesInitializer : MigrateDatabaseToLatestVersion<CoursesContext, Configuration>
    {
        //// usuwa i inicjalizuje dane w bazie danych od nowa
        //protected override void Seed(CoursesContext context)
        //{
        //    SeedCoursesData(context);
        //    base.Seed(context);
        //}

        public static void SeedCoursesData(CoursesContext context)
        {
            var categories = new List<Category>
            {
                new Category() { CategoryId=1, CategoryName="JavaScript 2.0", CategoryDescription="kurs javascript"},
                new Category() { CategoryId=2, CategoryName="Android 6.0", CategoryDescription="kurs nauki android"},
                new Category() { CategoryId=3, CategoryName="iOS", CategoryDescription="kurs nauki iOS"},
                new Category() { CategoryId=4, CategoryName="PHP", CategoryDescription="kurs nauki php"},
                new Category() { CategoryId=5, CategoryName="Python", CategoryDescription="kurs nauki python"},
                new Category() { CategoryId=6, CategoryName="HTML", CategoryDescription="kurs nauki HTML"},

            };

            categories.ForEach(c => context.Categories.AddOrUpdate(c));
            context.SaveChanges();

            var courses = new List<Course>
            {
                new Course() { CourseId=1, CourseAuthor="Jak Kowalski", CourseTitle="Java zaawansowane programowanie", CoursePrice=10,DiscountPercentage=50, CourseDescrpition="Opis kursu Jana k", CategoryId=1, Bestseller=true, DateAdded=DateTime.Now, ImageFileName="p-1.jpg", Recommended=true },
                new Course() { CourseId=2, CourseAuthor="Jak Kowalski", CourseTitle="Java zaawansowane programowanie", CoursePrice=10,DiscountPercentage=50, CourseDescrpition="Opis kursu Jana k", CategoryId=2, Bestseller=true, DateAdded=DateTime.Now, ImageFileName="p-1.jpg", Recommended=false },
                new Course() { CourseId=3, CourseAuthor="Adam Nowak", CourseTitle="Android zaawansowane programowanie", CoursePrice=10, CourseDescrpition="Adnroid dla opornych", CategoryId=3, Bestseller=true, DateAdded=DateTime.Now, ImageFileName="p-1.jpg", Recommended=true},
                new Course() { CourseId=4, CourseAuthor="Marian Inny", CourseTitle="PHP programowanie", CoursePrice=20,DiscountPercentage=10, CourseDescrpition="PHP progmrowanie", CategoryId=4, Bestseller= true, DateAdded=DateTime.Now, ImageFileName="p-1.jpg", Recommended=true },
                new Course() { CourseId=5, CourseAuthor="Elżbieta Kowalski", CourseTitle="iOS podstawy", CoursePrice=40, CourseDescrpition="iOS Kurs podstatowy", CategoryId=3, Bestseller=true, DateAdded=DateTime.Now, ImageFileName="p-1.jpg", Recommended=false },
                new Course() { CourseId=6, CourseAuthor="Adam Inny", CourseTitle="c# programowanie", CoursePrice=30,DiscountPercentage=20, CourseDescrpition="C# progmrowanie", CategoryId=4, Bestseller= true, DateAdded=DateTime.Now, ImageFileName="p-1.jpg", Recommended=true },
                new Course() { CourseId=7, CourseAuthor="Janek Kowalski", CourseTitle="HTML podstawy", CoursePrice=90, CourseDescrpition="HTML podstatowy", CategoryId=6, Bestseller=true, DateAdded=DateTime.Now, ImageFileName="p-1.jpg", Recommended=false },

            };

            courses.ForEach(a => context.Courses.AddOrUpdate(a));
            context.SaveChanges();

            var roles = new List<Role>
            {
                new Role() { Id=1, Name = "Customer" }
            };

            roles.ForEach(a => context.Roles.AddOrUpdate(u=>u.Name,a));
            context.SaveChanges();

           

        }
    }
}