﻿using CoursesStore.DAL;
using CoursesStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoursesStore.Infrastructure
{
    public class BasketManager
    {
        private CoursesContext db;
        private ISessionManager session;

        public BasketManager(ISessionManager session, CoursesContext db)
        {
            this.session = session;
            this.db = db;
        }

        public List<BasketItem> GetBasket()
        {
            List<BasketItem> basket;

            if (session.Get<List<BasketItem>>(Consts.BasketSessionKey) == null)
            {
                basket = new List<BasketItem>();
            }
            else
            {
                basket = session.Get<List<BasketItem>>(Consts.BasketSessionKey) as List<BasketItem>;
            }

            return basket;
        }

        public void AddBasket(int courseId)
        {
            var basket = GetBasket();
            //sprawdza czy istnieje w koszyku
            var basketItem = basket.Find(c => c.Course.CourseId == courseId);

            var addCourse = db.Courses.Where(c => c.CourseId == courseId).SingleOrDefault();

            if (basketItem == null)
            {
                if (addCourse != null)
                {
                    var newBasketItem = new BasketItem()
                    {
                        Course = addCourse,
                        Value = GetPriceAfterDiscount(addCourse),
                        Count = 1
                    };
                    basket.Add(newBasketItem);
                }
            }
            else
            {
                basketItem.Count++;
            }

            session.Set(Consts.BasketSessionKey, basket);
        }

       

        public void UpdateQuantity(int courseId, int quantity)
        {
            var basket = GetBasket();

            var basketItem = basket.Find(c => c.Course.CourseId == courseId);

            basketItem.Count = quantity;

            session.Set(Consts.BasketSessionKey, basket);
        }

        public int RemoveItem(int courseId)
        {

            var basket = GetBasket();
            var basketItem = basket.Find(c => c.Course.CourseId == courseId);

            basket.Remove(basketItem);
            if (basketItem == null)
            {
                if (basketItem.Count > 1)
                {
                    basketItem.Count--;
                }
                else
                {
                    basket.Remove(basketItem);
                }
            }
            return 0;
        }

        public Order CreateOrder(Order newOrder)
        {
            var basket = GetBasket();
            newOrder.DateOrder = DateTime.Now;
            db.Orders.Add(newOrder);
            newOrder.OrderItems = new List<OrderItem>();

            decimal basketValue = 0;

            foreach (var elemnt in basket)
            {
                var newItem = new OrderItem()
                {
                    Count = elemnt.Count,
                    OrderPrice = elemnt.Course.CoursePrice,
                    CourseId = elemnt.Course.CourseId
                };

                basketValue = (elemnt.Course.CoursePrice + basketValue) * elemnt.Count;
                newOrder.OrderItems.Add(newItem);
            }
            newOrder.OrderValue = basketValue;
            db.SaveChanges();

            return newOrder;
        }

        public decimal SumBasketPriceAfterDiscount
        {
            get
            {
                var AllSum = GetBasket();
                var Suma = AllSum.Sum(s => GetPriceAfterDiscount(s.Course) * s.Count);

                return Suma;
            }
        }

        public decimal SumBasketPrice
        {
            get
            {
                var AllSum = GetBasket();
                var Suma = AllSum.Sum(s => s.Course.CoursePrice * s.Count);

                return Suma;
            }
        }

        public int CountBasketItem()
        {
            var CountItem = GetBasket();
            var All = CountItem.Sum(s => s.Count);

            return All;
        }

        private decimal GetPriceAfterDiscount(Course addCourse)
        {
            return addCourse.CoursePrice - ((addCourse.CoursePrice / 100) * addCourse.DiscountPercentage); 
        }
    }
}