﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web;

namespace CoursesStore.Infrastructure
{
    public class AppConfig
    {
        private static string _imgFolder = ConfigurationManager.AppSettings["ImgFolder"];

        public static string ImgFolder
        {
            get
            {
                return _imgFolder;
            }
        }
    }
}