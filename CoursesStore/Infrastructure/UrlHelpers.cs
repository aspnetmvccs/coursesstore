﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoursesStore.Infrastructure
{
    public static class UrlHelpers
    {
        public static string MyImagePath(this UrlHelper helper, string imgName)
        {
            var ImageFolder = AppConfig.ImgFolder;
            var sciezka = Path.Combine(ImageFolder, imgName);
            var pathImg = helper.Content(sciezka);

            return pathImg;
        }
    }
}