﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoursesStore.Infrastructure
{
    public class Consts
    {
        public const string NewsCache = "NewsCachePara";
        public const string CategoryCache = "CategoryCachePara";
        public const string BasketSessionKey = "BasketSessionKey";
    }
}