﻿using CoursesStore.DAL;
using CoursesStore.Models;
using MvcSiteMapProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoursesStore.Infrastructure
{
    public class CoursesDetailsDynamicNode : DynamicNodeProviderBase
    {
        private CoursesContext db = new CoursesContext();

        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode nodee)
        {
            var returnValue = new List<DynamicNode>();

            foreach (Course course in db.Courses)
            {
                DynamicNode node = new DynamicNode();
                node.Title = course.CourseTitle;
                node.Key = "Course_" + course.CourseId;
                node.ParentKey = "Category_" + course.CategoryId;
                node.RouteValues.Add("coursesId", course.CourseId);
                returnValue.Add(node);
            }

            return returnValue;
        }
    }
}