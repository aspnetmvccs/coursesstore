namespace CoursesStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRecommended : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Course", "Recommended", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Course", "Recommended");
        }
    }
}
